# [Minimal Pairs Product Sum](https://www.sololearn.com/learn/4562/?ref=app) #

A problem set by [Burey](https://www.sololearn.com/Profile/197327/?ref=app) on [SoloLearn](https://www.sololearn.com).
  * Links only work if accessed from device with the SoloLearn app installed.
  * Problem text copied without permission verbatim at time of copying.  All credit to the above and to whoever they credit.


## Problem ##
Write a function that receives an array of itengers and returns the minimal sum of the array (sum of products of each two adjacent numbers).

### Example ###
Without sorting the array [40, 25, 10, 5,1], the sum is:
(40*25) + (25*10) + (10*5) + (5*1) = 1305

The challenge is to find the best possible sort of the array elements, to have the minimal sum result.

The expected output for the array [40, 25, 10, 5,1] is 225.

### Bonus ###
Return the resulted sorted array with the sum.