# Playground #

This playground repository will contain nothing exciting for most who stumble
upon it.  Its existance is simply to allow me some space to play with using
Git to manage code and as a a place to store solutions to puzzles I find to
help me learn software development.


## Aims ##

 1. To learn C#
 2. To apply C# learning to solve small problems and puzzles
 3. To improve code managment skills using Git
 4. To improve code managment skills using Bitbucket
 5. To find a open sourse development project to join or develop something
    substantional of my own


## Resource ##

 1. [SoloLearn](https://www.sololearn.com/)
    * Offers a primer on C# and other languages
    * Gameifies code comprehention in timed contents against other memebers
    * Offers logic puzzles to solve
 2. [CodeWars](https://www.codewars.com/)
    * Assumes understanding of at least one langauge as no teaching is offered
    * Offers logic puzzles to solve and code review by other members
 3. [Google Coding Competitions](https://codingcompetitions.withgoogle.com/)
    * Three competitions (Jam, Hash and Kick) on offer each year.
    * Past year problems availble would make good exercises.